package edu.gamm.marketplacewebapp.ControllerTest;

import edu.gamm.marketplacewebapp.Controller.AuthController;
import edu.gamm.marketplacewebapp.DTO.AccountDTO;
import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Security.JWTGenerator;
import edu.gamm.marketplacewebapp.repository.AccountRepository;
import edu.gamm.marketplacewebapp.repository.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

@DataJpaTest
//@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AuthControllerTest {

    @Autowired
    private AccountRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    AuthController authController;
    @DirtiesContext
    @Test
    public void AuthControllerTest_register() throws Exception {
        AccountDTO testuser = new AccountDTO("test", "123456", "test@test.no");
        System.out.println(authController.register(testuser).toString());
        for (Account acc:userRepository.findAll()) {
            System.out.println(acc.getUsername());
        }
        Assertions.assertTrue(userRepository.existsByUsername("test"));
    }

}
