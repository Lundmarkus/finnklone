package edu.gamm.marketplacewebapp.RepositoryTests;


import edu.gamm.marketplacewebapp.Entities.Role;
import edu.gamm.marketplacewebapp.repository.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;


    @Test
    public void RoleRepository_findsByName(){
        Role role1 = new Role("TEST_EXISTS");
        Role role2 = new Role("TEST_NOT_EXISTS");

        roleRepository.save(role1);

        Assertions.assertTrue(roleRepository.findByName("TEST_EXISTS").isPresent());
        Assertions.assertTrue(roleRepository.findByName("TEST_EXISTS").get().getName().equals("TEST_EXISTS"));

        Assertions.assertFalse(roleRepository.findByName("TEST_NOT_EXISTS").isPresent());
    }


    @Test
    public void RoleRepository_findsPresetRoles() {
        //Roles are preset on init, will always be in repo

        Assertions.assertTrue(roleRepository.findByName("USER").get().getName().equals("USER"));

        Assertions.assertTrue(roleRepository.findByName("ADMIN").get().getName().equals("ADMIN"));
    }
}
