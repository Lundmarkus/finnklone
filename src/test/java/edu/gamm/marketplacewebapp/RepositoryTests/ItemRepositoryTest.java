package edu.gamm.marketplacewebapp.RepositoryTests;

import edu.gamm.marketplacewebapp.Entities.Item;
import edu.gamm.marketplacewebapp.repository.ItemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ItemRepositoryTest {

    @Autowired
    ItemRepository itemRepository;

    //This test might be pointless
    @Test
    public void ItemRepository_AccessReturnsCorrectClass() {
        Item item = new Item();
        item.setItemName("TestItem");

        itemRepository.save(item);

        List<Item> items = itemRepository.findAll();
        Assertions.assertInstanceOf(Item.class, items.get(0));
        Assertions.assertEquals(item.getItemName(), items.get(0).getItemName());
    }


}
