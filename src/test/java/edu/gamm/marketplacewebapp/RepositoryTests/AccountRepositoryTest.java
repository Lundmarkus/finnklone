package edu.gamm.marketplacewebapp.RepositoryTests;


import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Role;
import edu.gamm.marketplacewebapp.MockUtil;
import edu.gamm.marketplacewebapp.repository.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
//@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void AccountRepository_Returns_Saved(){
        Account mockAccount = new Account("test", "123", "test@test.no", MockUtil.getDefaultRoles(), null, null);

        Assertions.assertFalse(accountRepository.existsByUsername("test"));

        accountRepository.save(mockAccount);

        Assertions.assertTrue(accountRepository.existsByUsername("test"));
    }


    @Test
    public void AccountRepository_Finds_By_Username(){
        Account mockAccount1 = new Account("test", "123", "test@test.no", MockUtil.getDefaultRoles(), null, null);
        //Account mockAccount2 = new Account("test", "123", "test@test.no", mockRole, null, null, null);

        //accepted if valid user with name 'test' is not found

        Assertions.assertFalse(accountRepository.findByUsername("test").isPresent());

        accountRepository.save(mockAccount1);
        //accountRepository.save(mockAccount2);
        //accountRepository.findByUsername("test"); //THIS IS A PROBLEM, USERNAME REALLY SHOULD BE UNIQUE

        //accepted if valid user with name 'test' is found
        Assertions.assertTrue(accountRepository.findByUsername("test").isPresent());

        //accepted if valid user with name 'test' is equal to added account
        Assertions.assertTrue(accountRepository.findByUsername("test").get().equals(mockAccount1));
    }

    @Test
    public void AccountRepository_FindsAccountsWithUsername(){
        //TODO: make one mock setup
        Account mockAccount1 = new Account("test", "123", "test@test.no", MockUtil.getDefaultRoles(), null, null);
        //Account mockAccount2 = new Account("test", "123", "test@test.no", mockRole, null, null, null);

        accountRepository.save(mockAccount1);
        //accepted if valid user with name 'test' is found

        List<Account> accounts = accountRepository.findAccountWithUsername("test");
        Assertions.assertSame(accounts.get(0), mockAccount1);
    }
}