package edu.gamm.marketplacewebapp;

import edu.gamm.marketplacewebapp.Entities.Role;

import java.util.ArrayList;
import java.util.List;

public class MockUtil {

    /**
     * gets default roles for Account
     * @return List<Role>
     */
    public static List<Role> getDefaultRoles(){

        List<Role> mockRole = new ArrayList<>();
        mockRole.add(new Role("USER"));
        mockRole.add(new Role("ADMIN"));

        return mockRole;
    }
}
