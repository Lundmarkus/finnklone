package edu.gamm.marketplacewebapp.ServiceTest;


import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Role;
import edu.gamm.marketplacewebapp.MockUtil;
import edu.gamm.marketplacewebapp.Service.AccountService;
import edu.gamm.marketplacewebapp.repository.AccountRepository;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AccountServiceTest {


    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountService accountService;



    @BeforeEach
    public void cleanDB(){
        accountRepository.deleteAll();
    }

    @DirtiesContext //this apparently messes up the database
    //Pass if all accounts are retrieved from database
    @Test
    public void AccountService_getAllAccount(){

        Account account1 = new Account("test1", "123", "test1@test.no", MockUtil.getDefaultRoles(), null, null);
        Account account2 = new Account("test2", "123", "test2@test.no", MockUtil.getDefaultRoles(), null, null);

        List<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);

        accountRepository.save(account1);
        accountRepository.save(account2);

        List<Account> result = accountService.getAllAccount();
        Assertions.assertEquals(accounts, result);
    }
    @DirtiesContext
    @Test
    public void AccountService_getAccountById(){
        Account account1 = new Account("test1", "123", "test1@test.no", MockUtil.getDefaultRoles(), null, null);
        Account account2 = new Account("test2", "123", "test2@test.no", MockUtil.getDefaultRoles(), null, null);

        //List<Account> accounts = new ArrayList<>();
        //accounts.add(account1);
        //accounts.add(account2);

        accountRepository.save(account1);
        //accountRepository.save(account2);
        //Database IDs start on 1, account 1 in this case
        Account result = accountService.getAccountById(1);
        System.out.println(result.getUsername());
        Assertions.assertTrue(account1.equals(result)); //DO NOT CHANGE TO 'assertEquals()'
    }
    @DirtiesContext
    @Test
    public void AccountService_createAccount(){

        Account account1 = new Account("test1", "123", "test1@test.no", MockUtil.getDefaultRoles(), null, null);
        Account account2 = new Account("test2", "123", "test2@test.no", MockUtil.getDefaultRoles(), null, null);

        List<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);

        accountService.createAccount(account1);
        accountService.createAccount(account2);

        List<Account> result = accountService.getAllAccount();
        Assertions.assertEquals(accounts, result);
    }
    @DirtiesContext
    @Test
    public void AccountService_updateAccount(){

        Account account1 = new Account("test1", "123", "test1@test.no", MockUtil.getDefaultRoles(), null, null);
        Account account2 = new Account("test2", "123", "test2@test.no", MockUtil.getDefaultRoles(), null, null);

        accountService.createAccount(account1);
        accountService.createAccount(account2);

        accountService.updateAccount(1, account2);
        Account result = accountService.getAccountById(1);

        Assertions.assertTrue(
                account1.getUsername().equals(account2.getUsername())
                        &&
                        account1.getUserEmail().equals(account2.getUserEmail())
                        &&
                        account1.getPassword().equals(account2.getPassword())
        );

    }

    @DirtiesContext
    @Test
    public void AccountService_deleteAccount (){

        Account account1 = new Account("test1", "123", "test1@test.no", MockUtil.getDefaultRoles(), null, null);

        accountService.createAccount(account1);

        accountService.deleteAccount(1);

        Assertions.assertTrue(accountService.getAllAccount().size() == 0);
    }

    @Test
    public void AccountService_getChatsForAccount (){

    }
    @Test
    public void AccountService_getItemsForAccount (){

    }
    @Test
    public void AccountService_getArchivedItemsForAccount (){

    }
    @Test
    public void AccountService_getFavoriteItems (){

    }


}
