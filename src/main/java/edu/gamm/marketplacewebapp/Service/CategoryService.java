package edu.gamm.marketplacewebapp.Service;

import java.util.ArrayList;
import java.util.List;

import edu.gamm.marketplacewebapp.Entities.Category;
import edu.gamm.marketplacewebapp.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for category related operations
 */
@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> getAllCategories() {
        return new ArrayList<>(categoryRepository.findAll());
    }

    public Category getCategoryById(int id) {
        if (categoryRepository.findById(id).isPresent()) {
            return categoryRepository.findById(id).get();
        } else {
            return null;
        }
    }

    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

    public Category updateCategory(int id, Category category) {
        Category category1 = getCategoryById(id);
        category1.setName(category.getName());
        return categoryRepository.save(category1);
    }

    public void deleteCategory(int id) {
        categoryRepository.deleteById(id);
    }
}