package edu.gamm.marketplacewebapp.Service;

import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Favorite;
import edu.gamm.marketplacewebapp.Entities.Item;
import edu.gamm.marketplacewebapp.repository.AccountRepository;
import edu.gamm.marketplacewebapp.repository.FavoriteRepository;
import edu.gamm.marketplacewebapp.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service class for account related operations
 */
@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    FavoriteRepository favoriteRepository;

    public List<Account> getAllAccount() {
        return new ArrayList<>(accountRepository.findAll());
    }

    public Account getAccountById(int id) {
        if (accountRepository.findById(id).isPresent()) {
            return accountRepository.findById(id).get();
        } else {
            return null;
        }
    }

    public Account createAccount(Account account) {
        return accountRepository.save(account);
    }

    public Account updateAccount(int id, Account account) {
        Account account1 = getAccountById(id);
        account1.setUserEmail(account.getUserEmail());
        account1.setUsername(account.getUsername());
        account1.setPassword(account.getPassword());
        return accountRepository.save(account1);
    }

    public void deleteAccount(int id) {
        accountRepository.deleteById(id);
    }

    public List<Item> getItemsForAccount(int id) {
        if (accountRepository.findById(id).isPresent()) {
            return new ArrayList<>(accountRepository.findById(id).get().getItems());
        } else {
            return null;
        }
    }

    public List<Item> getArchivedItemsForAccount(int id, boolean bool) {
        if (accountRepository.findById(id).isPresent()) {
            List<Item> allItems = new ArrayList<>(accountRepository.findById(id).get().getItems());
            List<Item> archivedItems = new ArrayList<>();
            allItems.forEach(item -> {
                if (bool) {
                    if (item.isArchived()) {
                        archivedItems.add(item);
                    }
                } else {
                    if (!item.isArchived()) {
                        archivedItems.add(item);
                    }
                }
            });
            return archivedItems;
        } else {
            return null;
        }
    }

    public List<Item> getFavoriteItems(int id) {
        if (accountRepository.findById(id).isPresent()) {
            List<Favorite> favorites = new ArrayList<>(accountRepository.findById(id).get().getFavorites());
            List<Item> favoriteItems = new ArrayList<>();
            favorites.forEach(favorite -> favoriteItems.add(favorite.getItem()));
            return favoriteItems;
        } else {
            System.out.println("user does not exist");
            return null;
        }
    }

    public Item addFavorite(int id, int itemId) {
        if (accountRepository.findById(id).isPresent() && itemRepository.findById(itemId).isPresent()) {
            if (!isFavorite(id, itemId)) {
                Account user = accountRepository.findById(id).get();
                Set<Favorite> favorites = new HashSet<>(user.getFavorites());
                Favorite favorite = new Favorite(user, itemRepository.findById(itemId).get());
                favorites.add(favorite);
                user.setFavorites(favorites);
                accountRepository.save(user);
                return favorite.getItem();
            }
        }
        return null;
    }

    public void removeFavorite(int id, int itemId) {
        if (accountRepository.findById(id).isPresent() && itemRepository.findById(itemId).isPresent()) {
            Account user = accountRepository.findById(id).get();
            Set<Favorite> favorites = new HashSet<>(user.getFavorites());
            favorites.forEach(favorite -> {
                if (favorite.getItem().getItemId() == itemId && favorite.getUserAccount().getId() == id) {
                    favoriteRepository.removeFavoriteFromUser(favorite.getFavoriteId());
                }
            });
        }
    }

    public boolean isFavorite(int id, int itemId) {
        if (accountRepository.findById(id).isPresent() && itemRepository.findById(itemId).isPresent()) {
            Account user = accountRepository.findById(id).get();
            Set<Favorite> favorites = new HashSet<>(user.getFavorites());
            List<Favorite> favoriteList = new ArrayList<>();
            favorites.forEach(favorite -> {
                if (favorite.getItem().getItemId() == itemId && favorite.getUserAccount().getId() == id) {
                    favoriteList.add(favorite);
                }
            });

            return favoriteList.size() > 0;
        }
        return false;
    }

}
