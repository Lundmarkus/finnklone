package edu.gamm.marketplacewebapp.Service;

import edu.gamm.marketplacewebapp.Entities.Item;
import edu.gamm.marketplacewebapp.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class for account related operations
 */
@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public List<Item> getAllItems() {
        List<Item> publishedItems = new ArrayList<>();
        List<Item> allItems = new ArrayList<>(itemRepository.findAll());
        allItems.forEach(item -> {
            if (!item.isArchived()) {
                publishedItems.add(item);
            }
        });
        return publishedItems;
    }

    public Item getItemById(int id) {
        if (itemRepository.findById(id).isPresent()) {
            return itemRepository.findById(id).get();
        } else {
            return null;
        }
    }

    public Item createItem(Item item) {
        return itemRepository.save(item);
    }

    public Item updateItem(int id, Item item) {
        Item item1 = getItemById(id);
        item1.setLongitude(item.getLongitude());
        item1.setItemName(item.getItemName());
        item1.setCategory(item.getCategory());
        item1.setBuyerAccount(item.getBuyerAccount());
        item1.setLatitude(item.getLatitude());
        item1.setPrice(item.getPrice());
        item1.setLongDescription(item.getLongDescription());
        item1.setShortDescription(item.getShortDescription());
        item1.setItemImage(item.getItemImage());
        item1.setArchived(item.isArchived());
        return itemRepository.save(item1);
    }

    public void deleteItem(int id) {
        itemRepository.deleteById(id);
    }
}
