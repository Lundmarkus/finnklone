package edu.gamm.marketplacewebapp.Service;

import edu.gamm.marketplacewebapp.Entities.ItemImage;
import edu.gamm.marketplacewebapp.repository.ItemImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

/**
 * Service class for image related operations
 */
@Service
public class ItemImageService {

    @Autowired
    private ItemImageRepository itemImageRepository;

    public int uploadImage(MultipartFile file) throws IOException {

        ItemImage itemImage = itemImageRepository.save(ItemImage.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .imageData(file.getBytes()).build());

        return itemImage.getId();
    }

    public ItemImage getItemImageById(int id) throws IOException {
        Optional<ItemImage> dbImage = itemImageRepository.findById(id);

        return ItemImage.builder().id(dbImage.get().getId())
                .name(dbImage.get().getName())
                .type(dbImage.get().getType())
                .imageData(dbImage.get().getImageData()).build();
    }

    public byte[] getImageById(int id) {
        Optional<ItemImage> dbImage = itemImageRepository.findById(id);

        byte[] image = dbImage.get().getImageData();
        return image;
    }

}
