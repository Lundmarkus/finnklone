package edu.gamm.marketplacewebapp.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Class item to represent items
 */
@Entity
//@Table(name = {table name}) // uncomment in case of differing class and table names
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemId;
    private String itemName;
    private String shortDescription;

    @Column(length = 2500)
    private String longDescription;
    private double price;
    private double latitude;
    private double longitude;
    private String address;
    private boolean archived;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seller_user_id")
    private Account sellerAccount;

    @ManyToOne
    @JoinColumn(name = "buyer_user_id")
    private Account buyerAccount;

    @OneToOne
    @JoinColumn(name = "image_id")
    private ItemImage itemImage;

    public Item() {

    }

    public Item(String itemName, String shortDescription, String longDescription, double price, double latitude, double longitude, String address, boolean archived, Category category, Account sellerAccount, Account buyerAccount, ItemImage itemImage) {
        this.itemName = itemName;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.price = price;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.archived = archived;
        this.category = category;
        this.sellerAccount = sellerAccount;
        this.buyerAccount = buyerAccount;
        this.itemImage = itemImage;
    }

    public int getItemId() {
        return itemId;
    }



    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Account getSellerAccount() {
        return sellerAccount;
    }

    public void setSellerAccount(Account sellerAccount) {
        this.sellerAccount = sellerAccount;
    }

    public Account getBuyerAccount() {
        return buyerAccount;
    }

    public void setBuyerAccount(Account buyerAccount) {
        this.buyerAccount = buyerAccount;
    }

    public ItemImage getItemImage() {
        return itemImage;
    }

    public void setItemImage(ItemImage itemImage) {
        this.itemImage = itemImage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}
