package edu.gamm.marketplacewebapp.Entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jdk.jfr.DataAmount;

import javax.persistence.*;
import java.util.*;

/**
 * Account entity that represents a user
 */
@Entity
@Table(name = "Account") // uncomment in case of differing class and table names
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private String userEmail;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Role> roles = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "sellerAccount")
    private Set<Item> items = new HashSet<>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "userAccount")
    private Set<Favorite> favorites = new HashSet<>();

    public Account() {

    }

    public Account(String username, String password, String userEmail, List<Role> roles, Set<Item> items, Set<Favorite> favorites) {
        this.username = username;
        this.password = password;
        this.userEmail = userEmail;
        this.roles = roles;
        this.items = items;
        this.favorites = favorites;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Set<Favorite> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<Favorite> favorites) {
        this.favorites = favorites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id && username.equals(account.username) && password.equals(account.password) && Objects.equals(userEmail, account.userEmail) && roles.equals(account.roles) && Objects.equals(items, account.items) && Objects.equals(favorites, account.favorites);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, userEmail, roles, items, favorites);
    }
}
