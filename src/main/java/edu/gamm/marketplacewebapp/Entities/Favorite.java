package edu.gamm.marketplacewebapp.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Class to represent favorite relations between items and accounts
 */
@Entity
//@Table(name = {table name}) // uncomment in case of differing class and table names
public class Favorite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int favoriteId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Account userAccount;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    public Favorite() {

    }

    public Favorite(Account userAccount, Item item) {
        this.userAccount = userAccount;
        this.item = item;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public Account getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(Account userAccount) {
        this.userAccount = userAccount;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
