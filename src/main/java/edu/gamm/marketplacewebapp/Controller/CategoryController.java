package edu.gamm.marketplacewebapp.Controller;

import edu.gamm.marketplacewebapp.Entities.Category;
import edu.gamm.marketplacewebapp.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class to define category api calls
 */
@RequestMapping("/api")
@RestController
@CrossOrigin("http://localhost:8081")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    /**
     * Method to get all categories
     * @return A list with categories
     */
    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getAllCategories() {
        return new ResponseEntity<>(categoryService.getAllCategories(), HttpStatus.OK);
    }

    /**
     * Method to get a category by id
     * @param id The id
     * @return A category
     */
    @GetMapping("/categories/{id}")
    public ResponseEntity<Category> getCategoryById(@PathVariable("id") int id) {
        System.out.println("Getting category " + id);
        return new ResponseEntity<>(categoryService.getCategoryById(id), HttpStatus.OK);
    }

    /**
     * Method to post a new category
     * @param category The category to post
     * @return The category
     */
    @PostMapping("/categories")
    public ResponseEntity<Category> postCategory(@RequestBody Category category) {
        return new ResponseEntity<>(categoryService.createCategory(category), HttpStatus.CREATED);
    }

    /**
     * Method to update a categories
     * @param id The category id
     * @param category The updated category
     * @return The updated category
     */
    @PutMapping("/categories/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable("id") int id, @RequestBody Category category) {
        return new ResponseEntity<>(categoryService.updateCategory(id,category), HttpStatus.OK);
    }

    /**
     * Method to delete category
     * @param id The category id
     * @return no content
     */
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<HttpStatus> deleteCategory(@PathVariable("id") int id) {
        categoryService.deleteCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
