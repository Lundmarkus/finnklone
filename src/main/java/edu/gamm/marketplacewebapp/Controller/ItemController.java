package edu.gamm.marketplacewebapp.Controller;

import edu.gamm.marketplacewebapp.DTO.ItemSellerUserDTO;
import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Item;
import edu.gamm.marketplacewebapp.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class to handle item related api calls
 */
@RequestMapping("/api")
@RestController
@CrossOrigin("http://localhost:8081")
public class ItemController {

    @Autowired
    ItemService itemService;

    /**
     * Method to get all items
     * @return A list of all items
     */
    @GetMapping("/items")
    public ResponseEntity<List<Item>> getAllItems() {
        return new ResponseEntity<>(itemService.getAllItems(), HttpStatus.OK);
    }

    /**
     * Method to get an item by id
     * @param id The item id
     * @return The item
     */
    @GetMapping("/items/{id}")
    public ResponseEntity<Item> getItemById(@PathVariable("id") int id) {
        return new ResponseEntity<>(itemService.getItemById(id), HttpStatus.OK);
    }

    /**
     * Method to post an item, this method requires token
     * @param item The item to post
     * @return The posted item
     */
    @PostMapping("/createItem")
    public ResponseEntity<Item> postItem(@RequestBody Item item) {
        return new ResponseEntity<>(itemService.createItem(item), HttpStatus.CREATED);
    }

    /**
     * Method to update an item, requires token
     * @param id The item id
     * @param item The updated item
     * @return The updated item
     */
    @PutMapping("/updateItem/{id}")
    public ResponseEntity<Item> updateItem(@PathVariable("id") int id, @RequestBody Item item) {
        return new ResponseEntity<>(itemService.updateItem(id,item), HttpStatus.OK);
    }

    /**
     * Method to delete an item
     * @param id The item id
     * @return no content
     */
    @DeleteMapping("/items/{id}")
    public ResponseEntity<HttpStatus> deleteItem(@PathVariable("id") int id) {
        itemService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Method to get the seller account for an item
     * @param id The item id
     * @return An object with the return arguments
     */
    @GetMapping("/items/{id}/seller")
    public ResponseEntity<ItemSellerUserDTO> getItemSellerAccount(@PathVariable("id") int id) {
        System.out.println("getting seller account");
        System.out.println("for item id: " + id);
        Account user = itemService.getItemById(id).getSellerAccount();
        ItemSellerUserDTO seller = new ItemSellerUserDTO(user.getId(),user.getUsername(),user.getUserEmail());
        return new ResponseEntity<>(seller, HttpStatus.OK);
    }
}
