package edu.gamm.marketplacewebapp.Controller;

import edu.gamm.marketplacewebapp.DTO.AccountDTO;
import edu.gamm.marketplacewebapp.DTO.AuthResponseDTO;
import edu.gamm.marketplacewebapp.DTO.LoginDTO;
import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Role;
import edu.gamm.marketplacewebapp.Security.JWTGenerator;
import edu.gamm.marketplacewebapp.Security.SecurityConstants;
import edu.gamm.marketplacewebapp.repository.AccountRepository;
import edu.gamm.marketplacewebapp.repository.RoleRepository;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Auth controller to handle authorization requests, does not require token
 */
@RestController
@RequestMapping("/api/auth")
@CrossOrigin("http://localhost:8081")
public class AuthController {


    private AuthenticationManager authenticationManager;

    private AccountRepository userRepository;

    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private JWTGenerator jwtGenerator;

    @Autowired
    public AuthController(
            AuthenticationManager authenticationManager,
            AccountRepository userRepository,
            RoleRepository roleRepository,
            PasswordEncoder passwordEncoder,
            JWTGenerator jwtGenerator)
    {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtGenerator = jwtGenerator;
    }

    /**
     * Method to register a new account
     * @param registerDto An object with the registration arguments
     * @return A string with info
     */
    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody AccountDTO registerDto) {
        System.out.println("given username: " + registerDto.getUsername());
        if(userRepository.existsByUsername(registerDto.getUsername())){
            return new ResponseEntity<>("Username already used by another account!", HttpStatus.BAD_REQUEST);
        }
        if(registerDto.getPassword().length() < 6) {
            return new ResponseEntity<>("Password is too short!", HttpStatus.BAD_REQUEST);
        }

        Account account = new Account();
        account.setUsername(registerDto.getUsername());

        //Password is encoded here
        account.setPassword(passwordEncoder.encode(registerDto.getPassword()));

        account.setUserEmail(registerDto.getEmail());
        Role roles = roleRepository.findByName("USER").get();
        account.setRoles(Collections.singletonList(roles));
        userRepository.save(account);

        if (userRepository.existsByUsername(account.getUsername())){
            System.out.println("Account existance verified!");
        }

        return new ResponseEntity<>("Account added successfully", HttpStatus.OK);
    }

    /**
     * Method to login
     * @param loginDTO An object with the login arguments
     * @return A response object
     */
    @PostMapping("/login")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody LoginDTO loginDTO) {
        System.out.println("username: " + loginDTO.getUsername());
        System.out.println("password: " + loginDTO.getPassword());
        System.out.println("Starting authentication");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getUsername(),
                        loginDTO.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        //JWT TOKEN SETUP
        String token = jwtGenerator.generateToken(authentication);
        return new ResponseEntity<>(new AuthResponseDTO(token), HttpStatus.OK);
    }

    /**
     * Method to get the Account for a token
     * @param token The token
     * @return The account
     */
    @PostMapping("/user")
    public ResponseEntity<Account> getUsername(@RequestBody String token) {
        List<Account> account = userRepository.findAccountWithUsername(jwtGenerator.getUsernameFromJWT(token));
        if (account.size() == 1) {
            return new ResponseEntity<>(account.get(0), HttpStatus.OK);
        } else {
            System.out.println("no users found for token");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
