package edu.gamm.marketplacewebapp.Controller;

import edu.gamm.marketplacewebapp.Entities.ItemImage;
import edu.gamm.marketplacewebapp.Service.ItemImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Class to handle image related api calls
 */
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081")
@RestController
public class ItemImageController {

    @Autowired
    private ItemImageService itemImageService;

    /**
     * Method to upload an image
     * @param file A multipart file to upload
     * @return The id given for the image that was uploaded
     * @throws IOException if an exception occurred
     */
    @PostMapping("/createImage")
    public ResponseEntity<Integer> uploadImage(@RequestBody MultipartFile file) throws IOException {
        int response = itemImageService.uploadImage(file);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * Method to get an image by its id
     * @param id The image id
     * @return The image
     * @throws IOException If an exception occurred
     */
    @GetMapping("/images/{id}/image")
    public ResponseEntity<?> getImageById(@PathVariable("id") int id) throws IOException {
        byte[] image = itemImageService.getImageById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }

    /**
     * Method to get image info
     * @param id The image id
     * @return Image info
     * @throws IOException If an exception occurred
     */
    @GetMapping("/images/{id}")
    public ResponseEntity<?> getItemImageById(@PathVariable("id") int id) throws IOException {
        ItemImage image = itemImageService.getItemImageById(id);

        return ResponseEntity.status(HttpStatus.OK).body(image);
    }
}
