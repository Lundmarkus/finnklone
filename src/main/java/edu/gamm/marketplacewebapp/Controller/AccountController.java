package edu.gamm.marketplacewebapp.Controller;

import edu.gamm.marketplacewebapp.Entities.Account;
import edu.gamm.marketplacewebapp.Entities.Item;
import edu.gamm.marketplacewebapp.Service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class AccountController to define api call paths, /users requires token
 */
@RequestMapping("/api")
@RestController
@CrossOrigin("http://localhost:8081")
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * Gets all accounts in database
     * @return A list of accounts
     */
    @GetMapping("/users")
    public ResponseEntity<List<Account>> getAllAccounts() {
        return new ResponseEntity<>(accountService.getAllAccount(), HttpStatus.OK);
    }

    /**
     * Gets account with id
     * @param id The account id
     * @return One account with the given id
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<Account> GetAccountById(@PathVariable("id") int id) {
        System.out.println("Getting account " + id);
        return new ResponseEntity<>(accountService.getAccountById(id), HttpStatus.OK);
    }

    /**
     * Method to create an account
     * @param account The account to create
     * @return The account that was created
     */
    @PostMapping("/users")
    public ResponseEntity<Account> postAccount(@RequestBody Account account) {
        Account account1 = accountService.createAccount(account);
        return new ResponseEntity<>(account1, HttpStatus.CREATED);
    }

    /**
     * Method to update an account
     * @param id The account id
     * @param account The updated version of this account
     * @return The account
     */
    @PutMapping("/users/{id}")
    public ResponseEntity<Account> updateAccount(@PathVariable("id") int id, @RequestBody Account account) {
        return new ResponseEntity<>(accountService.updateAccount(id,account), HttpStatus.OK);
    }

    /**
     * Method to delete an account
     * @param id the account id
     * @return no content
     */
    @DeleteMapping("/users/{id}")
    public ResponseEntity<HttpStatus> deleteAccount(@PathVariable("id") int id) {
        accountService.deleteAccount(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Method to get all items owned by an account
     * @param id The account id
     * @return A list of items
     */
    @GetMapping("users/{id}/items")
    public ResponseEntity<List<Item>> getItems(@PathVariable("id") int id) {
        return new ResponseEntity<>(accountService.getItemsForAccount(id), HttpStatus.OK);
    }

    /**
     * Method to get favorites for an account
     * @param id The account id
     * @return A list of items
     */
    @GetMapping("users/{id}/favorites")
    public ResponseEntity<List<Item>> getFavorites(@PathVariable("id") int id) {
        return new ResponseEntity<>(accountService.getFavoriteItems(id), HttpStatus.OK);
    }

    /**
     * Method to add a new favorite item for an account
     * @param id The account id
     * @param itemId The item id
     * @return An item
     */
    @PostMapping("users/{id}/favorites/{itemId}")
    public ResponseEntity<?> addFavorite(@PathVariable("id") int id, @PathVariable("itemId") int itemId) {
        return new ResponseEntity<>(accountService.addFavorite(id, itemId), HttpStatus.OK);
    }

    /**
     * Method to remove a favorite for an account
     * @param id The account id
     * @param itemId The item id
     * @return No content
     */
    @DeleteMapping("users/{id}/favorites/item/{itemId}")
    public ResponseEntity<HttpStatus> removeFavorite(@PathVariable("id") int id, @PathVariable("itemId") int itemId) {
        accountService.removeFavorite(id, itemId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Method to check if an item is a favorite for the given account
     * @param id The given account id
     * @param itemId The item id
     * @return true or false
     */
    @GetMapping("users/{id}/item/{itemId}/isFavorite")
    public  ResponseEntity<?> isFavorite(@PathVariable("id") int id, @PathVariable("itemId") int itemId) {
        return new ResponseEntity<>(accountService.isFavorite(id, itemId), HttpStatus.OK);
    }

    /**
     * Method to get all archived or non-achieved items for a given account
     * @param id The account id
     * @param bool A boolean, true to get archived items or false to get non-archived items
     * @return A list of items
     */
    @GetMapping("users/{id}/items/archived/{bool}")
    public ResponseEntity<List<Item>> getItemsArchived(@PathVariable("id") int id, @PathVariable("bool") boolean bool) {
        List<Item> items = accountService.getArchivedItemsForAccount(id,bool);
        if (items != null) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
