package edu.gamm.marketplacewebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.gamm.marketplacewebapp.Entities.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

}