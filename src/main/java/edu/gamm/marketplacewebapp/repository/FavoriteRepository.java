package edu.gamm.marketplacewebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.gamm.marketplacewebapp.Entities.Favorite;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite,Integer> {

    @Modifying
    @Transactional
    @Query("DELETE FROM Favorite t WHERE t.favoriteId=?1")
    void removeFavoriteFromUser(int id);

}