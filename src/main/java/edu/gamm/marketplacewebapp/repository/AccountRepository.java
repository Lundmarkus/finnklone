package edu.gamm.marketplacewebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.gamm.marketplacewebapp.Entities.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {
    //TODO: add username to account
    Optional<Account> findByUsername(String username);
    Boolean existsByUsername(String username);

    @Query("SELECT t FROM Account t WHERE t.username=?1")
    List<Account> findAccountWithUsername(String username);
}
