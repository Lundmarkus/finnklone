package edu.gamm.marketplacewebapp.DTO;

/**
 * Data transfer object to send item seller account info
 */
public class ItemSellerUserDTO {

    private int id;
    private String username;
    private String userEmail;


    public ItemSellerUserDTO(int id, String username, String userEmail) {
        this.id = id;
        this.username = username;
        this.userEmail = userEmail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String email) {
        this.userEmail = email;
    }
}
