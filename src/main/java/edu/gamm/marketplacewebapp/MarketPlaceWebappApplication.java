package edu.gamm.marketplacewebapp;

import edu.gamm.marketplacewebapp.Entities.*;
import edu.gamm.marketplacewebapp.Service.*;
import edu.gamm.marketplacewebapp.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;

@ComponentScan(basePackages = "edu.gamm.marketplacewebapp.*")
@EnableJpaRepositories(basePackages = "edu.gamm.marketplacewebapp.repository")
@SpringBootApplication
@EnableSwagger2
//@EnableWebMvc
public class MarketPlaceWebappApplication implements CommandLineRunner {

	@Resource
	FileStorageService storageService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	AccountService accountService;

	@Autowired
	ItemService itemService;

	public static void main(String[] args)  {
		SpringApplication.run(MarketPlaceWebappApplication.class, args);

	}
	@Autowired
	RoleRepository roleRepository;


	@Override
	public void run(String... arg) throws Exception {
		System.out.println("RUNNING");
		roleRepository.deleteAll();
		roleRepository.save(new Role("USER"));
		roleRepository.save(new Role("ADMIN"));
		storageService.init();

		categoryService.createCategory(new Category("Bicycle"));
		categoryService.createCategory(new Category("House"));
		categoryService.createCategory(new Category("Car"));
		categoryService.createCategory(new Category("Furniture"));
		categoryService.createCategory(new Category("Computers"));
	}
}