import { createStore } from "vuex";
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  //asyncStorage: true,
});

export default createStore({
  state: {
    userToken: "",
    loggedInStatus: false,
    item: {
      byerAccount: null,
      sellerAccount: null,
      id: null,
      category: {
        categoryId: null,
      },
      itemName: "",
      shortDescription: "",
      longDescription: "",
      price: null,
      address: "",
      latitude: null,
      longitude: null,
      itemImage: null,
    },
  },
  getters: {
    token: (state) => () => state.userToken,
  },
  mutations: {
    CHANGE_USERTOKEN(state, newToken) {
      state.userToken = newToken;
    },
    CHANGE_LOGGED_IN_STATUS(state, newLoggedInStatus) {
      state.loggedInStatus = newLoggedInStatus;
    },
    SET_ITEM(state, value) {
      this.state.item = value;
    },
  },
  actions: {
    updateItem({ state, commit }, value) {
      if (state.item) {
        commit("SET_ITEM", value);
      }
      console.log(this.state.item);
    },
  },
  modules: {},
  plugins: [vuexLocal.plugin],
});
