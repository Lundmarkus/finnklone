import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ItemView from "../views/ItemView.vue";
import CreateItemView from "../views/CreateItemView.vue";
import LoginView from "../views/LoginView.vue";
import FavoritesView from "../views/FavoritesView.vue";
import PublishedItemsView from "../views/PublishedItemsView.vue";
import UserPageView from "../views/UserPageView.vue";
import UpdateItemView from "../views/UpdateItemView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/item/:id",
    name: "item",
    props: true,
    component: ItemView,
  },
  {
    path: "/create-item",
    name: "createItemView",
    component: CreateItemView,
  },
  {
    path: "/update-item/:id",
    name: "updateItemView",
    component: UpdateItemView,
    props: true,
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/favorites",
    name: "favorites",
    component: FavoritesView,
  },
  {
    path: "/published-items",
    name: "published",
    component: PublishedItemsView,
  },
  {
    path: "/user-account",
    name: "account",
    component: UserPageView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
