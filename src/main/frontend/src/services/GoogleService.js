import axios from "axios";

export default {
  async getAddress(lat, lon) {
    return await axios
      .get(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
          lat +
          "," +
          lon +
          "&key=AIzaSyCumhXakAOagVe1_4R-9gjlzw9OFE3V24Y"
      )
      .then((response) => {
        if (response.data.error_message) {
          console.log(response.data.error_message);
        } else {
          if (response.data.results.length != 0) {
            let addressFormat = "";
            let address;
            response.data.results.forEach((element) => {
              if (element.types[0] == "postal_code") {
                address = element.address_components;
              }
            });
            if (!address) {
              return "Adressen ble ikke funnet";
            }
            address.forEach((element) => {
              if (element.types[0] == "postal_code") {
                addressFormat += element.long_name += " ";
              }
              if (element.types[0] == "postal_town") {
                addressFormat += element.short_name;
              }
            });
            return addressFormat;
          }
        }
      });
  },
  async getCoordinates(address) {
    return await axios
      .get(
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
          address +
          "&key=AIzaSyCumhXakAOagVe1_4R-9gjlzw9OFE3V24Y"
      )
      .then((response) => {
        if (response.data.error_message) {
          console.log(response.data.error_message);
        } else {
          const location = response.data.results[0].geometry.location;
          return {
            latitude: location.lat,
            longitude: location.lng,
          };
        }
      });
  },
};
