import axios from "axios";
import store from "@/store/index.js";
import EventService from "./EventService";

const apiClient = axios.create({
  baseURL: "http://localhost:8080/api/auth",
  withCredentials: false,
  headers: {
    Accept: "application/json",
  },
});

export default {
  async logIn(username, password) {
    return await apiClient
      .post("/login", {
        username: username,
        password: password,
      })
      .then((response) => {
        return response.data; //returns token
      })
      .catch(() => {
        return "user do not exist";
      });
  },

  async getCurrentUser() {
    return await apiClient
      .post("/user", store.getters.token())
      .then((response) => {
        return response.data; //returns user account or null if not found
      })
      .catch((error) => {
        console.log(error);
      });
  },

  async signUp(username, email, password) {
    return await apiClient
      .post("/register", {
        username: username,
        email: email,
        password: password,
      })
      .then((response) => {
        console.log(response);
        return response.data; //returns token
      })
      .catch((error) => {
        console.log(error);
      });
  },

  async getUserChats() {
    return await this.getCurrentUser().then((response) => {
      const userid = response.id;
      EventService.getTokenApiClient()
        .get("/users/" + userid + "/chats")
        .then((response) => {
          return response.data; //should return list of chats
        })
        .catch((error) => console.log(error));
    });
  },

  async getUserItems() {
    return await this.getCurrentUser().then((response) => {
      const userid = response.id;
      EventService.getTokenApiClient()
        .get("/users/" + userid + "/items")
        .then((response) => {
          return response.data; //should return list of items
        })
        .catch((error) => console.log(error));
    });
  },

  async getUserArchivedItems(isArchived) {
    return await this.getCurrentUser().then((response) => {
      const userid = response.id;
      EventService.getTokenApiClient()
        .get("users/" + userid + "/items/archived/" + isArchived)
        .then((response) => {
          return response.data; //returns list of items
        })
        .catch((error) => console.log(error));
    });
  },

  async getUserFavoriteItems(userId) {
    return await EventService.getTokenApiClient()
      .get("users/" + userId + "/favorites")
      .then((response) => {
        return response.data; //should return list of favorites
      });
  },

  async isItemUserFavorite(itemId, userId) {
    return await EventService.getTokenApiClient()
      .get("users/" + userId + "/item/" + itemId + "/isFavorite")
      .then((response) => {
        return response.data;
      });
  },

  async addFavoriteToUser(itemId, userId) {
    return await EventService.getTokenApiClient()
      .post("users/" + userId + "/favorites/" + itemId)
      .then((response) => {
        return response.data; // should return item
      })
      .catch((error) => {
        console.log(error);
      });
  },

  async deleteFavoriteFromUser(itemId, userId) {
    return await EventService.getTokenApiClient()
      .delete("users/" + userId + "/favorites/item/" + itemId)
      .then((response) => {
        return response; // should return nothing
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
