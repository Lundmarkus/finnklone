import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:8080/api",
  withCredentials: false,
  headers: {
    Accept: "application/json",
  },
});

export default {
  //category api calls:
  getCategories() {
    return apiClient.get("/categories");
  },
  getCategory(Id) {
    return apiClient.get("/categories/" + Id);
  },
  addCategory(name) {
    return apiClient.post("/categories/", {
      name: name,
    });
  },
  updateCategory(Id, name) {
    return apiClient.put("/categories/" + Id, {
      name: name,
    });
  },
  deleteCategory(Id) {
    return apiClient.delete("/categories/" + Id);
  },
};
