import axios from "axios";
import AuthenticateService from "./AuthenticateService";
import CategoryService from "./CategoryService";
import store from "@/store/index.js";

const apiClient = axios.create({
  baseURL: "http://localhost:8080/api",
  withCredentials: false,
  headers: {
    Accept: "application/json",
  },
});

export default {
  //token client
  getTokenApiClient() {
    return axios.create({
      baseURL: "http://localhost:8080/api",
      withCredentials: false,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${store.getters.token()}`,
      },
    });
  },

  //User api calls:
  getUsers() {
    return this.getTokenApiClient().get("/users");
  },
  getUser(Id) {
    return this.getTokenApiClient().get("/users/" + Id);
  },
  deleteUser(Id) {
    return this.getTokenApiClient().delete("/users/" + Id);
  },

  //Item api calls:
  getItems() {
    return apiClient.get("/items");
  },
  getItem(id) {
    return apiClient.get("/items/" + id);
  },
  getItemSeller(id) {
    return apiClient.get("/items/" + id + "/seller");
  },

  addItem(item, id, categoryId) {
    AuthenticateService.getCurrentUser()
      .then((response) => {
        let userData = response;

        CategoryService.getCategory(categoryId)
          .then((response) => {
            let categoryData = response.data;

            this.getItemImage(id)
              .then((response) => {
                let itemImageData = response.data;

                this.getTokenApiClient()
                  .post("/createItem", {
                    itemName: item.itemName,
                    shortDescription: item.shortDescription,
                    longDescription: item.longDescription,
                    price: item.price,
                    latitude: item.latitude,
                    longitude: item.longitude,
                    sellerAccount: userData,
                    category: categoryData,
                    itemImage: itemImageData,
                    address: item.address,
                    archived: item.archived,
                  })
                  .catch((error2) => {
                    console.log(error2);
                  });
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  },
  updateItem(id, item) {
    this.getTokenApiClient().put("/updateItem/" + id, item);
  },
  deleteItem(Id) {
    return apiClient.delete("/items/" + Id);
  },

  // Item image api calls
  getItemImage(id) {
    return apiClient.get("/images/" + id);
  },
  getImage(id) {
    return apiClient.get("/images/" + id + "/image");
  },
  postImage(image) {
    return axios.post("http://localhost:8080/api/createImage", image, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${store.getters.token()}`,
      },
    });
  },
  getFavorites(id) {
    alert("GETTING FAVORITES");
    return this.getTokenApiClient().post("/users/favorites", id);
  },
};
