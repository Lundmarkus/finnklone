beforeEach(() => {
  cy.visit("http://localhost:8081/");
});

describe("If user is not logged in", () => {
  it("Display login link in top nav bar, instead of chat link", () => {
    cy.get("button#chats").should("not.exist");
  });
});
