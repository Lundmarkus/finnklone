beforeEach(() => {
  cy.visit("http://localhost:8081/create-item");
});

describe("submit button disabled status", () => {
  it("submit button is disabled on page load", () => {
    cy.get("button#submit").should("be.disabled");
  }),
    it("submit button is disabled if only image is missing", () => {
      // Set all fields valid except image
      cy.get("input#title").click({ force: true }).type("test title");
      cy.get("select#category").select(0, { force: true });
      cy.get("input#price").click({ force: true }).type("123");
      cy.get("textarea#short-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("textarea#long-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("input#location").click({ force: true }).type("Trondheim");

      cy.get("button#submit").should("be.disabled");
    }),
    it("submit button is disabled if only address is missing", () => {
      // Set all fields valid except address
      cy.get("input#title").click({ force: true }).type("test title");
      cy.get("select#category").select(0, { force: true });
      cy.get("input#price").click({ force: true }).type("123", { force: true });
      cy.get("textarea#short-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("textarea#long-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("input[type=file]").selectFile(
        {
          contents: Cypress.Buffer.from("file contents"),
          fileName: "file.png",
          lastModified: Date.now(),
        },
        { force: true }
      );

      cy.get("button#submit").should("be.disabled");
    }),
    it("submit button is active if all fields are valid", () => {
      // Set all fields valid
      cy.get("input#title").click({ force: true }).type("test title");
      cy.get("select#category").select(0, { force: true });
      cy.get("input#price").click({ force: true }).type("123");
      cy.get("textarea#short-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("textarea#long-description")
        .click({ force: true })
        .type("test titleawdawd");
      cy.get("input[type=file]").selectFile(
        {
          contents: Cypress.Buffer.from("file contents"),
          fileName: "file.png",
          lastModified: Date.now(),
        },
        { force: true }
      );
      cy.get("input#location").click({ force: true }).type("Trondheim");

      cy.get("button#submit").should("not.be.disabled");
    });
});

describe("form validation", () => {
  it("Show error message if title is invalid", () => {
    cy.get("input#title").click({ force: true }).type("a");

    cy.get(".input-errors").should("not.have.css", "display", "none");
  });
  it("Show error message if shortDescription is invalid", () => {
    cy.get("textarea#short-description")
      .click({ force: true })
      .type("asdfdsasd");

    cy.get(".input-errors").should("not.have.css", "display", "none");
  });
  it("Show error message if longDescription is invalid", () => {
    cy.get("textarea#long-description")
      .click({ force: true })
      .type("asdfdsasd");

    cy.get(".input-errors").should("not.have.css", "display", "none");
  });
});

describe("successfull submition", () => {
  it("popup box is shown", () => {
    // Set all fields valid
    cy.get("input#title").click({ force: true }).type("test title");
    cy.get("select#category").select(0, { force: true });
    cy.get("input#price").click({ force: true }).type("123");
    cy.get("textarea#short-description")
      .click({ force: true })
      .type("test titleawdawd");
    cy.get("textarea#long-description")
      .click({ force: true })
      .type("test titleawdawd");
    cy.get("input[type=file]").selectFile(
      {
        contents: Cypress.Buffer.from("file contents"),
        fileName: "file.png",
        lastModified: Date.now(),
      },
      { force: true }
    );
    cy.get("input#location").click({ force: true }).type("Trondheim");

    cy.get("button#submit").click();

    cy.get("div#popup").should("have.css", "display", "block");
  });
});
