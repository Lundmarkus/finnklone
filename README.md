# Finnklone

## Installation
Follow these steps to install and run the application:

1. Download the project.
2. Run the command: mvn install
3. Navigate to src/main/frontend and run npm install.
4. navigate to src\main\java\edu\gamm\marketplacewebapp\MarketPlaceWebappApplication.java and run the class
5. In your browser, go to the address http://localhost:8081

## Diagrams
### Database ER-diagram
![Database diagram picture](https://gitlab.com/Lundmarkus/finnklone/uploads/420d15651d5a4fffa266c8e977601963/image.png)

### Database interaction

url, password and username for the MySql database is set in the "application.properties" file, within the resources folder.

the relevant fields are:

- spring.datasource.url=jdbc:mysql:{url}
- spring.datasource.username={username} 
- spring.datasource.password={password}

***

## Testing
To run frontend cypress tests, navigate to src/main/frontend and run the command: npx run cypress.

## Api documentation
swagger documentation can be accessed at this link when the backend is running:
http://localhost:8080/swagger-ui/#/

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment
